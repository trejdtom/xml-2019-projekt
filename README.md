# XML-2019-projekt

CIA World Factbook project for BI-XML (LS 2018/2019)

Autor: Tomáš Trejdl (trejdtom@fit.cvut.cz)

#### Vybrané státy:

- Europian Union: https://www.cia.gov/library/publications/the-world-factbook/geos/ee.html
- Cocos (Keeling) Islands: https://www.cia.gov/library/publications/the-world-factbook/geos/ck.html
- Sri Lanka: https://www.cia.gov/library/publications/the-world-factbook/geos/ce.html
- Singapore: https://www.cia.gov/library/publications/the-world-factbook/geos/sn.html

## Dev reqirements

- Saxon: http://saxon.sourceforge.net/
- FOP: https://xmlgraphics.apache.org/fop/download.html

## Validation

### DTD

```sh
$ xmllint --noout --dtdvalid ./validation/validation.dtd ./all.xml
```

### RelaxNG

```sh
jing ./validation/validation.rng all.xml
jing -c validation/validation.rnc source.xml
```


## Generating output

### HTML

Output will be generated in the /output folder

Run in linux shell!

```sh
$ cd transformations
$ ./transform-html.sh
```

### PDF

Output will be generated in the /output folder

Run in linux shell!

```sh
$ cd transformations
$ ./generate-pdf.sh
```

## Credits

- factbook to XML parser: https://gitlab.fit.cvut.cz/dvorat19/bi-xml-survivors/blob/master/parser/parser.jar