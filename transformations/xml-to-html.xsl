<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes" method="html" version="5.0" />
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
                <meta name="viewport" content="width=device-width,initial-scale=1" />
                <title>
                    <xsl:value-of select="country/@name" />
                </title>
                <link rel="stylesheet" href="style.css" type="text/css" />
            </head>
            <body>
                <nav>
                    <ul>
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a href="cocos-islands.html">Cocos Islands</a>
                        </li>
                        <li>
                            <a href="european-union.html">European Union</a>
                        </li>
                        <li>
                            <a href="singapore.html">Singapore</a>
                        </li>
                        <li>
                            <a href="sri-lanka.html">Sri Lanka</a>
                        </li>
                    </ul>
                </nav>
                <main class="content">
                    <xsl:apply-templates select="country" />
                </main>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="country">
        <h1>
            <xsl:value-of select="@name" />
        </h1>
        <img class="flag">
            <xsl:attribute name="src">
                <xsl:value-of select="concat('./img/', @countryCode, '-flag.gif')" />
            </xsl:attribute>
            <xsl:attribute name="alt">
                <xsl:value-of select="concat('Flaf of ', @name)" />
            </xsl:attribute>
        </img>
        <img class="map">
            <xsl:attribute name="src">
                <xsl:value-of select="concat('./img/', @countryCode, '-map.gif')" />
            </xsl:attribute>
            <xsl:attribute name="alt">
                <xsl:value-of select="concat('Map of ', @name)" />
            </xsl:attribute>
        </img>
        <p class="navigation__title">Navigation</p>
        <ul class="navigation">
            <xsl:for-each select="section">
                <li>
                    <a>
                        <xsl:attribute name="href">
                            <xsl:value-of select="concat('#', @name)" />
                        </xsl:attribute>
                        <xsl:value-of select="@name" />
                    </a>
                </li>
            </xsl:for-each>
        </ul>
        <xsl:apply-templates select="section" />
    </xsl:template>
    <xsl:template match="section">
        <article>
            <h2 class="section-title">
                <xsl:attribute name="id">
                    <xsl:value-of select="@name" />
                </xsl:attribute>
                <xsl:value-of select="@name" />
            </h2>
            <section>
                <xsl:apply-templates select="text" />
            </section>
        </article>
    </xsl:template>
    <xsl:template match="text">
        <h3 class="subsection-title">
            <xsl:value-of select="@name" />
        </h3>
        <xsl:choose>
            <xsl:when test="data/subdata">
                <ul>
                    <xsl:apply-templates select="*/subdata" />
                </ul>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="data" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="data">
        <p>
            <xsl:value-of select="." />
        </p>
    </xsl:template>
    <xsl:template match="subdata">
        <li>
            <xsl:value-of select="." />
        </li>
    </xsl:template>
</xsl:stylesheet>