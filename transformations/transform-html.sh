#!/bin/bash
for FILE in ../data/*.xml;
do
    echo "Transforming $FILE"
    BASENAME=`basename $FILE .xml`
    java -jar "../SaxonHE9-9-1-1J/saxon9he.jar" -s:$FILE -xsl:"./xml-to-html.xsl" -o:"../output/${BASENAME}.html"
done

cp ./index.html ../output;
cp ./style.css ../output;

if [ ! -d ../output/img ]; then
    mkdir ../output/img
fi


for IMG in ../data/flags/* ../data/maps/*;
do
    cp $IMG ../output/img
done

echo "All done.";