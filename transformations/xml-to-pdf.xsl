<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes" />
    <xsl:template match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4">
                    <fo:region-body margin="2cm" />
                    <fo:region-before extent="1cm" display-align="after" />
                    <fo:region-after extent="1cm" />
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="A4">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block font-size="10pt" font-weight="400" text-align="center">
                        CIA World Factbook
                    </fo:block>
                </fo:static-content>
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block font-size="10pt" font-weight="400" text-align="center">
                        Page
                        <fo:page-number />
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-size="18pt" font-weight="400" text-align="center" id="info">
                        World Factbook
                    </fo:block>
                    
                    <fo:list-block>
                        <xsl:for-each select="project/countries/country">
                            <fo:list-item>
                                <fo:list-item-label end-indent="label-end()">
                                    <fo:block>
                                        <fo:inline>&#183;</fo:inline>
                                    </fo:block>
                                </fo:list-item-label>
                                <fo:list-item-body start-indent="body-start()">
                                    <fo:block>
                                        <fo:basic-link internal-destination="{@countryCode}" color="blue" text-decoration="underline">
                                            <xsl:value-of select="@name" />
                                        </fo:basic-link>
                                    </fo:block>
                                </fo:list-item-body>
                            </fo:list-item>
                        </xsl:for-each>
                    </fo:list-block>
                    
                    <xsl:apply-templates />
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <xsl:template match="project/countries/country">
        <fo:block page-break-before="always" />
        <fo:block id="{@countryCode}" font-size="24pt" font-weight="bold" space-after="9pt" space-before="5pt" text-transform="capitalize">
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block text-align="center" padding-top="5pt">
            <fo:external-graphic height="40px" content-width="scale-to-fit" content-height="scale-to-fit" scaling="uniform">
                <xsl:attribute name="src">
                    <xsl:value-of select="concat('url(./data/flags/', @countryCode, '-flag.gif)')" />
                </xsl:attribute>
            </fo:external-graphic>
        </fo:block>
        
        <xsl:apply-templates select="section" />
    </xsl:template>
    <xsl:template match="section">
        <fo:block font-size="18pt" space-after="9pt" space-before="5pt" text-transform="capitalize">
            <xsl:value-of select="@name" />
        </fo:block>
        <xsl:apply-templates select="text" />
    </xsl:template>
    <xsl:template match="text">
        <fo:block font-size="14pt" space-after="9pt" space-before="5pt" text-transform="capitalize">
            <xsl:value-of select="@name" />
        </fo:block>
        <xsl:choose>
            <xsl:when test="data/subdata">
                <fo:block>
                    <xsl:apply-templates select="*/subdata" />
                </fo:block>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="data" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="data">
        <fo:block text-indent="1cm">
            <xsl:value-of select="." />
        </fo:block>
    </xsl:template>
    <xsl:template match="subdata">
        <fo:block>
            -
            <xsl:value-of select="." />
        </fo:block>
    </xsl:template>
</xsl:stylesheet>