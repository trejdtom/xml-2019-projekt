#!/bin/bash

if [ ! -d "../output" ]; then
    mkdir ../output
fi

../fop-2.3/fop/fop -xml '../all.xml' -xsl './xml-to-pdf.xsl' -pdf '../output/factbook.pdf'